Lebenslauf
==========

Schulbildung
------------

.. list-table::
   :widths: auto
   :header-rows: 0

   * - 1974 – 1978
     - AHS, Bundesrealgymnasium Wien 10
   * - 1978 – 1981
     - Höhere Technische Bundeslehranstalt für Elektrotechnik, Wien 10

Beruflicher Werdegang
---------------------

.. list-table::
   :widths: auto
   :header-rows: 0

   * - 2017 - dato
     - **Freiberuflicher Consultant**
   * - 2016 - 2017
     - **Einzelhandelskaufmann**
   * - 
     - monkey. moods Verlagsges.m.b.H
   * - 
     - 1070 Wien
   * - 2009 - 2016
     - **Freiberuflicher Consultant**
   * - 2007 – 2009
     - **Senior Consultant**
   * - 
     - imposult EDV-Dienstleistungen GmbH & Co KG
   * - 
     - 1090 Wien
   * - 2004 – 2006
     - **Selbständiger IT–Consultant**
   * - 2003
     - **Senior Consultant, Trainer**
   * - 
     - GNC - Global Network Communication
   * - 
     - 1190 Wien
   * - 1999 – 2002
     - **Senior Consultant**
   * - 
     - BLSG - Business- and Logistic Systems Ges.m.b.H.
   * - 
     - 1180 Wien
   * - 1997 – 1999
     - **Projektleiter, Entwicklungsleiter, Senior-Consultant**
   * - 
     - cmb - Control Management Beratungsges.m.b.H.
   * - 
     - 1050 Wien
   * - 1990 – 1997
     - **Selbständiger IT–Consultant**
   * - 1990
     - **Betriebsleiter**
   * - 
     - Squash Insel Süd Betriebsges.m.b.H.
   * - 
     - 1230 Wien
   * - 1988 – 1990
     - **EDV Leiter**
   * - 
     - Revita Datenverarbeitung
   * - 
     - 1230 Wien
   * - 1985 – 1988
     - **Organisationsleiter Stellvertreter**
   * - 
     - Adressen Suppan Direktmarketing
   * - 
     - 1230 Wien
   * - 1981 – 1985
     - **Programmierer / Organisationsprogrammierer**
   * - 
     - Rechenzentrum Schmidhuber
   * - 
     - 1050 Wien
