# Configuration file for the Sphinx documentation builder.

# -- Path setup --------------------------------------------------------------

#import sphinx_rtd_theme
from datetime import date

# -- Project information -----------------------------------------------------

project = 'Lebenslauf'
copyright = f'{date.today().year}, Helmut Fischer'
author = 'Helmut Fischer'

# The short X.Y version
version = '23.04'
# The full version, including alpha/beta/rc tags
release = '23.04.01'

# -- General configuration ---------------------------------------------------

extensions = [
    'sphinx.ext.autodoc',
    'sphinx_rtd_theme',
]

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
#source_suffix = '.rst'

# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_rtd_theme'

# -- Extension configuration -------------------------------------------------
