Fachwissen
==========

Allgemeine Managementaufgaben
-----------------------------
• Bedarfs-, System- und Ablaufanalyse, Initiierung neuer EDV-Systeme
• Projektleitung und -management
• Configuration Management, Rollout Management, Trainingsplanung
• Recruiting und Schulung
• Budgetierung, Budgetkontrolle und –verantwortung
• Qualitätsmanagement und Auditing
• Reporting

Branchen- und Bereichserfahrung
-------------------------------
EU Projekte (F&E), Binnenschifffahrt, Logistik, Banken, F&E, NGOs,
Ausbildung, Großhandel, Verlagswesen, Versandhandel, Spedition, Wirtschaftstreuhand,
Direktmarketing, Gastronomie, Einzelhandel

Betriebssysteme
---------------
Linux (alle gängigen Distributionen), (Unix), XENIX, (MVS/VSE/CICS), GCOS 6/7/8/9,
Windows alle seit 2.x (inklusive Server), Novell 3x/4x/5x, DOS ab 1.2

Hardware, Protokolle
--------------------
Netzwerke (Firewalls, IDS, Security Mechanismen, Router, Switches, Segmentierung,
LAN, WAN, WLAN, Verkabelung, Normen), Klimatisierung, TCP/IP (ftp, http, IPSec, SSH,
Telnet, Tunneling, VPN Technologien)

DB-/DC-Systeme
--------------
ORACLE, Progress, Informix, MS Access, Superbase, dBASE II / III / IV

Programmiersprachen
-------------------
PL/SQL, SQL, PHP, HTML, (Perl), (Python), (Java/Javascript), diverse Skriptsprachen, C++, C,
COBOL, PL/1, RPG II, PASCAL, Fortran, BASIC

Tools/Anwendungssoftware
------------------------
ORACLE Designer/Developer/Forms/Reports/JDeveloper, Sybase PowerDesigner,
diverse CASE Tools (Rational), Projektmanagement Tools, Bugtracking Tools, Office
Pakete unter Windows und Linux

Methoden
--------
Analyse, Datenbankmodellierung, System/Applikationsdesign, Objektorientierte Methoden,
Agile Softwareentwicklung (SCRUM), Strukturierte Programmierung, Security, CRM, CMS, ERM,
Training, Präsentation, Consulting, Qualitätssicherung/Auditing, Produktmanagement,
Troubleshooting, Hotline

Sprachen
--------
Deutsch (Muttersprache)
Englisch (verhandlungssicher)
