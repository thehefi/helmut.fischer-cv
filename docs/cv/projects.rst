Projekterfahrung (Auszug)
=========================

Übersicht wichtiger Projekte
----------------------------

=========== ==================================== ========================================
Zeitraum    Auftraggeber                         Projekt
=========== ==================================== ========================================
2007 - 2008 Statistik Austria                    Datawarehouse Registerprobezählung
2003        gnc – Global Network Communication   Content Management System
2003        GNC Akademie                         ORACLE Training
2003        gnc – Global Network Communication   ORACLE Consulting
2002        BLSG - Business Logisic Systems GmbH Konzept zur Unternehmensweiterführung
2002        via donau                            DoRIS - Donau River Information Service
2002        Europäische Union                    ETNA - European Transport Network Application
2001        DCS Danube Combined Services         DCSP – Danube Combined Services projects
2001        Europäische Union                    ALSO Danube
1999 - 2001 Siemens Business Services, München   CISÖ Cargo Information System Österreich
1999        BLSG - Business Logisic Systems GmbH Netzwerkinfrastruktur
1998 - 1999 cmb Control Management Beratung      CS++ Banken Controlling System
1994        ÖWWG                                 Studenten- und Projektverwaltung
1991 - 1993 Einhell AG                           Warenwirtschaftssystem mit Konzernkonsolidierung
1990        Adressen Suppan                      Datenmigration RDBMS
1989        Adressen Suppan                      Rechenzentrum-Übersiedlung
=========== ==================================== ========================================

Ausführliche Tabelle: `Google Docs <https://docs.google.com/spreadsheets/u/0/d/1zWh21BzhK6dFEMInVz9gisMxA_LZI5Qxw-phSzJtMtA//>`_

Kurzbeschreibung ausgewählter Projekte
--------------------------------------

Registerprobezählung
~~~~~~~~~~~~~~~~~~~~
Statistik Austria - Datawarehouse als Ersatz für die Volkszählung mit
Fragebogen; Desin DWH, ETL, Scripting, Versioning, Roll-out, Dokumentation,
Maintenance, Troubleshooting

CISÖ - Cargo Information System Österreich
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ÖBB - Güterverkehrsabrechnungssystem: Deltaanalyse Altsystem – Fremdsystem,
Analyse, Fachkonzept und begleitende Beratung bei der Realisation,
Rollout Management und Schulungsplanung in heterogenen Systemumgebungen (6 Unix Datenbank Server,
>100 Windows NT Application Server, >5000 Windows NT Clients)

ALSO Danube - Advanced Logistic Solutions for the Danube Waterway
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Europäische Kommission - Größtes Forschungs- und Entwicklungsprojekt im 5. Rahmenprogramm,
Subworkpackage Leader (Conceptual Design, Development, Implementation and
Systemconcepts CSL.DB - Common Source Logistics Database)

DoRIS Donau River Information System
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
via donau, Donau Transport- und Entwicklungsgesellschaft m.b.H. - Projektaudit, Qualitätskontrolle,
Review, Entwicklungs- und Planungsüberwachung

ETNA European Transport Network Application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Europäische Union - Datenbankapplikation mit grafischer Visualisierung von Transportketten unter
Berücksichtigung von Geodaten und Logistikeckdaten; Projektleitung
Softwareentwicklung

DCSP
~~~~
DCS Danube Combined Services Transportges.m.b.H. - Dispositions-, Abrechnungs- und Verfolgungssystem für den Gütertransport auf
Linienschiffen. Projekt- und
Entwicklungsleitung für Web- und Applikationsfrontend

Studenten- und Projektverwaltung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Österreichische Werbewissenschaftliche Gesellschaft an der WU Wien - Umstellung auf datenbankgestützte Client/Server Architektur;
Projektleitung

Banken Controlling System
~~~~~~~~~~~~~~~~~~~~~~~~~
cmb, Control Management Beratungsges.m.b.H. - Entwicklungsleitung, Teilprojektleitung Schnittstellen zu Mainframe Bankensystemen
(Delbrück & Co. Köln; Nationalbank Essen; Merck, Fink & Co. München; Wölbern
Hamburg, RZB Wien, Ungarn, Singapur)

Warenwirtschaftssystem mit Konzernkonsolidierung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Einhell AG - Gesamtprojekt- und Entwicklugsleitung (Einhell und Wieshofer Österreich; Einhell AG Deutschland,
Portugal, Ungarn, Finnland, China)

Directmarketing DBMS
~~~~~~~~~~~~~~~~~~~~
Adressen Suppan - Implementierung und Migration der Adressverwaltung Branchen- und Privatadressen auf
ORACLE Datenbanken Mainframesysteme. Projektleiter

Rechenzentrumsübersiedlung
~~~~~~~~~~~~~~~~~~~~~~~~~~
Adressen Suppan - Planung der Infrastruktur, Übersiedlung, Parallelbetrieb und Inbetriebnahme des gesamten
Rechenzentrums von Wien 5 nach Wien 23, Projektleitung

CMS System der GNC Gruppe
~~~~~~~~~~~~~~~~~~~~~~~~~
Global Network Comnunication - Homepages
(http://www.gnc.at, http://akademie.gnc.at, http://www.yourmail.at ).
Projektmanagement, Beratung für Design und Funktionalität, Realisierungsvorgaben.

GTIME
~~~~~
GNC Gruppe - Zeitdokumentation und CRM System,
Projektunterstützung, Troubleshooting im Web- und Applikationsfrontend
